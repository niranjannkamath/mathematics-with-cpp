#include <iostream>
#include "LinSolve.h"
#include "LinearAlgebra.h"

using namespace std;

LinSolve::LinSolve(){}
float* LinSolve::gaussElimination(float** aug, int eqns) {			//function to find solution to a system of linear equations
	float* soln = new float[eqns];									//using gauss elimination method
	float diag;
	for (int i = 0; i < eqns; i++) {
		if (*(aug[i] + i) == 0 && i != eqns - 1) {					//pivoting step
			LinearAlgebra::swapRows(aug, i, i + 1, eqns, eqns + 1);
		}
		diag = *(aug[i] + i);
		*(aug[i] + i) = 1;
		for (int j = i + 1; j < eqns + 1; j++) {
			*(aug[i] + j) /= diag;
			for (int k = i + 1; k < eqns; k++) {
				*(aug[k] + j) -= (*(aug[k] + i)) * (*(aug[i] + j));
			}
		}
	}

	for (int i = 0; i < eqns; i++) {
		soln[i] = *(aug[i] + eqns);
	}

	for (int i = eqns - 1; i >= 0; i--) {
		for (int j = eqns - 1; j > i; j--) {
			soln[i] -= (*(aug[i] + j)) * soln[j];
		}
	}

	return soln;
}

float* LinSolve::gaussJordanElimination(float** aug, int eqns) {	//function to find solution to a system of linear equations
	float* soln = new float[eqns];									//using gauss jordan elimination method
	float diag;
	for (int i = 0; i < eqns; i++) {
		diag = *(aug[i] + i);
		*(aug[i] + i) = 1;
		for (int j = i + 1; j < eqns + 1; j++) {
			*(aug[i] + j) /= diag;
			for (int k = 0; k < eqns; k++) {
				if (k != i) {
					*(aug[k] + j) -= (*(aug[k] + i)) * (*(aug[i] + j));
				}
			}
		}
	}

	for (int i = 0; i < eqns; i++) {
		soln[i] = *(aug[i] + eqns);
	}

	return soln;
}

float* LinSolve::LUDecomposition(float** coeff, int eqns, float* vec) {		//function to find solution to a system of linear equations
	float* soln = vec;												//using LU Decomposition method
	float** upper = new float* [eqns];
	float** lower = new float* [eqns];
	for (int i = 0; i < eqns; i++) {
		lower[i] = new float[eqns];
		lower[i][i] = 1;
		for (int j = 0; j < eqns; j++) {
			if (j > i) {
				lower[i][j] = 0;
			}
			else {
				lower[i][j] = *(coeff[i] + j);
			}
		}
	}

	for (int i = 0; i < eqns; i++) {
		upper[i] = new float[eqns];
		for (int j = 0; j < eqns; j++) {
			if (j < i) {
				upper[i][j] = 0;
			}
			else {
				upper[i][j] = *(coeff[i] + j);
			}
		}
	}

	for (int i = 0; i < eqns; i++) {
		for (int j = i; j < eqns; j++) {
			for (int k = 0; k < i; k++) {
				upper[i][j] -= lower[i][k] * upper[k][j];
			}
			if (j != i) {
				for (int k = 0; k < i; k++) {
					lower[j][i] -= lower[j][k] * upper[k][i];
				}
				lower[j][i] /= upper[i][i];
			}
		}
	}

	for (int i = 0; i < eqns; i++) {
		for (int j = 0; j < i; j++) {
			soln[i] -= (*(lower[i] + j)) * soln[j];
		}
	}

	for (int i = eqns - 1; i >= 0; i--) {
		for (int j = eqns - 1; j > i; j--) {
			soln[i] -= ((*(upper[i] + j)) * soln[j]);
		}
		soln[i] /= upper[i][i];
	}

	return soln;
}