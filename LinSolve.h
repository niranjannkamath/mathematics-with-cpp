#pragma once
#include <iostream>

class LinSolve {
public:LinSolve();
	  static float* gaussElimination(float** aug, int eqns);
	  static float* gaussJordanElimination(float** aug, int eqns);
	  static float* LUDecomposition(float** coeff, int eqns, float* vec);
};