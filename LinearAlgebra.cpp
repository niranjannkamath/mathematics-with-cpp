//this is an implementation file for LinearAlgebra.h

#include <iostream>
#include <cmath>
#include "D://Git/mathematics-with-cpp/LinearAlgebra.h"

using namespace std;

LinearAlgebra::LinearAlgebra() {}
	int LinearAlgebra::det(int** arr, int N) {																	//function to compute determinant
		int determinant = 0;
		int** minorArray;
		if (N == 1) {
			return *arr[0];
		}
		for (int i = 0; i < N; i++) {
			minorArray = minor(arr, N, 0, i);
			determinant += pow(-1, i) * (*(arr[0] + i)) * det(minorArray, N - 1);
		}
		return determinant;
	}

	float LinearAlgebra::det(float** arr, int N) {																//function to compute determinant
		float determinant = 0;
		float** minorArray;
		if (N == 1) {
			return *arr[0];
		}
		for (int i = 0; i < N; i++) {
			minorArray = minor(arr, N, 0, i);
			determinant += pow(-1, i) * (*(arr[0] + i)) * det(minorArray, N - 1);
		}
		return determinant;
	}

	double LinearAlgebra::det(double** arr, int N) {															//function to compute determinant
		double determinant = 0;
		double** minorArray;
		if (N == 1) {
			return *arr[0];
		}
		for (int i = 0; i < N; i++) {
			minorArray = minor(arr, N, 0, i);
			determinant += pow(-1, i) * (*(arr[0] + i)) * det(minorArray, N - 1);
		}
		return determinant;
	}

	int** LinearAlgebra::adjoint(int** arr, int N) {															//function to compute adjoint of the matrix
		int** cof = cofactor(arr, N);
		int** adj = new int* [N];
		for (int i = 0; i < N; i++) {
			adj[i] = new int[N];
			for (int j = 0; j < N; j++) {
				adj[i][j] = cof[j][i];
			}
		}
		return adj;
	}

	float** LinearAlgebra::adjoint(float** arr, int N) {														//function to compute adjoint of the matrix
		float** cof = cofactor(arr, N);
		float** adj = new float* [N];
		for (int i = 0; i < N; i++) {
			adj[i] = new float[N];
			for (int j = 0; j < N; j++) {
				adj[i][j] = cof[j][i];
			}
		}
		return adj;
	}

	double** LinearAlgebra::adjoint(double** arr, int N) {														//function to compute adjoint of the matrix
		double** cof = cofactor(arr, N);
		double** adj = new double* [N];
		for (int i = 0; i < N; i++) {
			adj[i] = new double[N];
			for (int j = 0; j < N; j++) {
				adj[i][j] = cof[j][i];
			}
		}
		return adj;
	}

	float** LinearAlgebra::inverse(int** arr, int N) {															//function to compute inverse of a matrix
		float determinant = (float)det(arr, N);
		float** null;
		if (determinant == 0) {
			cout << "The Inverse of the Matrix does not exist." << endl;
			return nullptr;
		}
		int** adj = adjoint(arr, N);
		float** inv = new float* [N];
		for (int i = 0; i < N; i++) {
			inv[i] = new float[N];
			for (int j = 0; j < N; j++) {
				inv[i][j] = (1 / determinant) * (float)adj[i][j];
			}
		}
		return inv;
	}

	float** LinearAlgebra::inverse(float** arr, int N) {														//function to compute inverse of a matrix
		float determinant = det(arr, N);
		float** null;
		if (determinant == 0) {
			cout << "The Inverse of the Matrix does not exist." << endl;
			return nullptr;
		}
		float** adj = adjoint(arr, N);
		float** inv = new float* [N];
		for (int i = 0; i < N; i++) {
			inv[i] = new float[N];
			for (int j = 0; j < N; j++) {
				inv[i][j] = (1 / determinant) * adj[i][j];
			}
		}
		return inv;
	}

	double** LinearAlgebra::inverse(double** arr, int N) {														//function to compute inverse of a matrix
		double determinant = det(arr, N);
		double** null;
		if (determinant == 0) {
			cout << "The Inverse of the Matrix does not exist." << endl;
			return nullptr;
		}
		double** adj = adjoint(arr, N);
		double** inv = new double* [N];
		for (int i = 0; i < N; i++) {
			inv[i] = new double[N];
			for (int j = 0; j < N; j++) {
				inv[i][j] = (1 / determinant) * adj[i][j];
			}
		}
		return inv;
	}

	void LinearAlgebra::printMatrix(float* arr, int N) {														//function for printing a float array
		for (int i = 0; i < N; i++) {
			cout << *(arr + i) << " ";
		}
		cout << endl;
	}

	void LinearAlgebra::printMatrix(int* arr, int N) {															//function for printing a float array
		for (int i = 0; i < N; i++) {
			cout << *(arr + i) << " ";
		}
		cout << endl;
	}

	void LinearAlgebra::printMatrix(double* arr, int N) {														//function for printing a float array
		for (int i = 0; i < N; i++) {
			cout << *(arr + i) << " ";
		}
		cout << endl;
	}

	int** LinearAlgebra::swapRows(int** matrix, int row1, int row2, int rows, int cols) {						//fucntion for swapping two rows of a integer type matrix
		int* temp = new int[cols];
		if (row1 < rows && row2 < rows) {
			for (int i = 0; i < cols; i++) {
				temp[i] = *(matrix[row1] + i);
				*(matrix[row1] + i) = *(matrix[row2] + i);
				*(matrix[row2] + i) = temp[i];
			}
		}
		else {
			cout << "Row index is out of bounds for swapping" << endl;
		}

		return matrix;
	}

	float** LinearAlgebra::swapRows(float** matrix, int row1, int row2, int rows, int cols) {						//fucntion for swapping two rows of a integer type matrix
		float* temp = new float[cols];
		if (row1 < rows && row2 < rows) {
			for (int i = 0; i < cols; i++) {
				temp[i] = *(matrix[row1] + i);
				*(matrix[row1] + i) = *(matrix[row2] + i);
				*(matrix[row2] + i) = temp[i];
			}
		}
		else {
			cout << "Row index is out of bounds for swapping" << endl;
		}

		return matrix;
	}

	double** LinearAlgebra::swapRows(double **matrix, int row1, int row2, int rows, int cols) {						//fucntion for swapping two rows of a integer type matrix
		double* temp = new double[cols];
		if (row1 < rows && row2 < rows) {
			for (int i = 0; i < cols; i++) {
				temp[i] = *(matrix[row1] + i);
				*(matrix[row1] + i) = *(matrix[row2] + i);
				*(matrix[row2] + i) = temp[i];
			}
		}
		else {
			cout << "Row index is out of bounds for swapping" << endl;
		}

		return matrix;
	}

	int** LinearAlgebra::swapCols(int** matrix, int col1, int col2, int rows, int cols) {						//fucntion for swapping two rows of a integer type matrix
		int* temp = new int[rows];
		if (col1 < cols && col2 < cols) {
			for (int i = 0; i < rows; i++) {
				temp[i] = *(matrix[i] + col1);
				*(matrix[i] + col1) = *(matrix[i] + col2);
				*(matrix[i] + col2) = temp[i];
			}
		}
		else {
			cout << "Column index is out of bounds for swapping" << endl;
		}

		return matrix;
	}

	float** LinearAlgebra::swapCols(float** matrix, int col1, int col2, int rows, int cols) {						//fucntion for swapping two rows of a integer type matrix
		float* temp = new float[rows];
		if (col1 < cols && col2 < cols) {
			for (int i = 0; i < rows; i++) {
				temp[i] = *(matrix[i] + col1);
				*(matrix[i] + col1) = *(matrix[i] + col2);
				*(matrix[i] + col2) = temp[i];
			}
		}
		else {
			cout << "Column index is out of bounds for swapping" << endl;
		}

		return matrix;
	}

	double** LinearAlgebra::swapCols(double** matrix, int col1, int col2, int rows, int cols) {						//fucntion for swapping two rows of a integer type matrix
		double* temp = new double [rows] ;
		if (col1 < cols && col2 < cols) {
			for (int i = 0; i < rows; i++) {
				temp[i] = *(matrix[i] + col1);
				*(matrix[i] + col1) = *(matrix[i] + col2);
				*(matrix[i] + col2) = temp[i];
			}
		}
		else {
			cout << "Column index is out of bounds for swapping" << endl;
		}

		return matrix;
	}

	int** LinearAlgebra::minor(int** arr, int N, int p, int q) {												//function to compute minor of an element is a matrix
		int** min;
		min = new int* [N - 1];
		for (int i = 0; i < N - 1; i++) {
			min[i] = new int[N - 1];
		}
		for (int i = 0; i < N; i++) {
			if (i == p) {
				continue;
			}
			for (int j = 0; j < N; j++) {
				if (j == q) {
					continue;
				}
				else if (i < p && j < q) {
					min[i][j] = *(arr[i] + j);
				}
				else if (i < p && j > q) {
					min[i][j - 1] = *(arr[i] + j);
				}
				else if (i > p&& j < q) {
					min[i - 1][j] = *(arr[i] + j);
				}
				else if (i > p&& j > q) {
					min[i - 1][j - 1] = *(arr[i] + j);
				}
			}
		}
		return min;
	}

	float** LinearAlgebra::minor(float** arr, int N, int p, int q) {												//function to compute minor of an element is a matrix
		float** min;
		min = new float* [N - 1];
		for (int i = 0; i < N - 1; i++) {
			min[i] = new float[N - 1];
		}
		for (int i = 0; i < N; i++) {
			if (i == p) {
				continue;
			}
			for (int j = 0; j < N; j++) {
				if (j == q) {
					continue;
				}
				else if (i < p && j < q) {
					min[i][j] = *(arr[i] + j);
				}
				else if (i < p && j > q) {
					min[i][j - 1] = *(arr[i] + j);
				}
				else if (i > p&& j < q) {
					min[i - 1][j] = *(arr[i] + j);
				}
				else if (i > p&& j > q) {
					min[i - 1][j - 1] = *(arr[i] + j);
				}
			}
		}
		return min;
	}

	double** LinearAlgebra::minor(double** arr, int N, int p, int q) {												//function to compute minor of an element is a matrix
		double** min;
		min = new double* [N - 1];
		for (int i = 0; i < N - 1; i++) {
			min[i] = new double[N - 1];
		}
		for (int i = 0; i < N; i++) {
			if (i == p) {
				continue;
			}
			for (int j = 0; j < N; j++) {
				if (j == q) {
					continue;
				}
				else if (i < p && j < q) {
					min[i][j] = *(arr[i] + j);
				}
				else if (i < p && j > q) {
					min[i][j - 1] = *(arr[i] + j);
				}
				else if (i > p&& j < q) {
					min[i - 1][j] = *(arr[i] + j);
				}
				else if (i > p&& j > q) {
					min[i - 1][j - 1] = *(arr[i] + j);
				}
			}
		}
		return min;
	}

	int** LinearAlgebra::cofactor(int** arr, int N) {																//function to compute the cofactor matrix
		int** cof = new int* [N];
		int** min;
		for (int i = 0; i < N; i++) {
			cof[i] = new int[N];
			for (int j = 0; j < N; j++) {
				min = minor(arr, N, i, j);
				cof[i][j] = pow(-1, i + j) * det(min, N - 1);
			}
		}
		return cof;
	}

	float** LinearAlgebra::cofactor(float** arr, int N) {															//function to compute the cofactor matrix
		float** cof = new float* [N];
		float** min;
		for (int i = 0; i < N; i++) {
			cof[i] = new float[N];
			for (int j = 0; j < N; j++) {
				min = minor(arr, N, i, j);
				cof[i][j] = pow(-1, i + j) * det(min, N - 1);
			}
		}
		return cof;
	}

	double** LinearAlgebra::cofactor(double** arr, int N) {															//function to compute the cofactor matrix
		double** cof = new double* [N];
		double** min;
		for (int i = 0; i < N; i++) {
			cof[i] = new double[N];
			for (int j = 0; j < N; j++) {
				min = minor(arr, N, i, j);
				cof[i][j] = pow(-1, i + j) * det(min, N - 1);
			}
		}
		return cof;
	}