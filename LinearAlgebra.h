#pragma once

#include <iostream>

class LinearAlgebra {
public:LinearAlgebra();
	  static int det(int** arr, int N);
	  static  float det(float** arr, int N);
	  static double det(double** arr, int N);
	  static int** adjoint(int** arr, int N);
	  static float** adjoint(float** arr, int N);
	  static double** adjoint(double** arr, int N);
	  static float** inverse(int** arr, int N);
	  static float** inverse(float** arr, int N);
	  static double** inverse(double** arr, int N);
	  static void printMatrix(float* arr, int N);
	  static void printMatrix(int* arr, int N);
	  static void printMatrix(double* arr, int N);
	  static int** swapRows(int** matrix, int row1, int row2, int rows, int cols);
	  static float** swapRows(float** matrix, int row1, int row2, int rows, int cols);
	  static double** swapRows(double** matrix, int row1, int row2, int rows, int cols);
	  static int** swapCols(int** matrix, int col1, int col2, int rows, int cols);
	  static float** swapCols(float** matrix, int col1, int col2, int rows, int cols);
	  static double** swapCols(double** matrix, int col1, int col22, int rows, int cols);

private:static int** minor(int** arr, int N, int p, int q);
	   static float** minor(float** arr, int N, int p, int q);
	   static double** minor(double** arr, int N, int p, int q);
	   static int** cofactor(int** arr, int N);
	   static float** cofactor(float** arr, int N);
	   static double** cofactor(double** arr, int N);
};
